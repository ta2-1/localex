﻿export var InsightsResx = {
    Dismiss: "Dismiss",
    NoRecommendationsText: "There is nothing to share right now. You will be notified when something requires your attention.",
    Affected: "Affected",
    NotAffected: "Not Affected",
    Timeline: "Timeline",
    ViewDetails: "View Details",
    PieChart: "Pie Chart",
    LineChart: "Line Chart",
    SwitchViewToGraph: "View in graph format",
    SwitchViewToTable: "View in tabular format",
    PreventInsight: "Prevent this insight from showing",
    For: "for"
}
export type IInsightsResx = typeof InsightsResx;