﻿export var SearchResx = {
    Results: "Results",
    SearchResultsFor: "search results for",
    Recommended: "Recommended",
    Search: "Search",
    SearchForServices: "Search for services",
    More: "More",
    Less: "Less",
    SortBy: "Sort by",
    Relevance: "Relevance",
    Price: "Price",
    EnrollingNow: "Enrolling now!",
    SearchQuery: "Search Query",
    SearchError: "Enter a search query"
}
export type ISearchResx = typeof SearchResx;