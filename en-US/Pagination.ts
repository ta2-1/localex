﻿export var PaginationResx = {
    Next: "Next",
    Previous: "Previous",
    NextPage: "Next page",
    PreviousPage: "Previous page",
    Page: "Page"
}
export type IPaginationResx = typeof PaginationResx;