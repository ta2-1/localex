﻿export var TagPickerResx = {
    RemoveTag: "Remove {0}",
    EnterTag: "Enter {0}",
    EnterTagLabel: "Type {0} and then press enter key to add",
    AddTag: "Add {0}",
    Tag: "Tag"
}
export type ITagPickerResx = typeof TagPickerResx;