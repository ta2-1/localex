﻿export var CustomerResx = {
    CustomersPageHeader: "Adding a new customer",
    CustomersPageSubHeader: "When creating a customer, select the contracts containing the customer’s entitlements. Create the customer, and then you can define independent workspaces where data will be restricted to the members of that workspace. If a new contract becomes available, you can return to this page and add the agreement at that time.",
    ContractStepTitle: "Step 1: Select a contract",
    EditCustomerNameTitle: "Step 2: Edit customer name",
    VerifyInformationTitle: "Step 3: Verify information",
    CustomerNameTitle: "Customer name",
    Next: "Next",
    Edit: "Edit",
    Cancel: "Cancel",
    CancelCreateCustomerButtonAria: "Cancel Create Customer Button",
    CancelEditCustomerButtonAria: "Cancel Edit Customer Button",
    CancelCreateWorkspaceButtonAria: "Cancel Create Workspace Button",
    CancelEditWorkspaceButtonAria: "Cancel Edit Workspace Button",
    AddCustomer: "Add Customer",
    EditCustomer: "Edit Customer",
    ContractStepInfo: "Select a contract from your available Clarify agreements",
    CustomerNameDescription: "Set a customer friendly name to be displayed inside the Services Hub",
    VerifyInformationDescription: "Confirm you have correctly defined the customer",
    VerifyCustomerLabel: "Customer:",
    VerifyContractsLabel: "Contract(s):",
    None: "None",
    ContractAttributeNumber: "Number:",
    ContractAttributeExpiration: "Expiration:",
    ContractAttributeCustomer: "Customer:",
    ScheduleNameTableHeader: "Schedule Name",
    ScheduleIdTableHeader: "Schedule Id",
    ScheduleStartDateTableHeader: "Schedule Start Date",
    ScheduleEndDateTableHeader: "Schedule End Date",
    ContractStepButtonAriaLabel: "Contract step button",
    CustomerNameStepButtonAriaLabel: "Customer name step button",
    AddCustomerBtnTitle: "Add customer button",
    AddCustomerBtnAriaLabel: "Add customer button",
    EditCustomerBtnTitle: "Edit customer button",
    EditCustomerBtnAriaLabel: "Edit customer button",
    AddCustomerTitle: "Congratulations on adding a new customer!",
    AddCustomerMessage: "{0} has been added as a new customer. Now that your customer page has been set up, you can manage your customer's workspaces and contracts, assign global administrators, share files and more. But first, let's create a workspace for your new customer.",
    CreateWorkspace: "Create workspace",
    CustomerPageNoContracts: "Sorry, there are no contract results",
    Contract: "Contract",
    ManageContracts: "Manage contracts",
    Add: "Add",
    AddAContractTitle: "Add a contract",
    AddAContractAria: "Add a contract",
    RemoveContract: "Remove",
    RemoveContractTitle: "Remove contract",
    RemoveContractAria: "Remove contract",
    RemoveContractConfirmation: "Please confirm you want to permanetly remove this contract from the customer.",
    RemoveAllContractsNotification: "Customers require a contract to remain active. Please select at least one contract.",
    RemovingAContractTitle: "Removing a contract",
    RemovingAContractConfirmation: "You are about to remove one or more contracts from {0} customer page, are you sure you want to do this?",
    Remove: "Remove",
    RemoveWorkspaceTitle: "Remove workspace",
    RemoveWorkspaceConfirmation: "You are about to remove one or more workspaces from {0}, are you sure you want to do this?",
    RemoveAllWorkspacesNotification: "Customers require a workspace to remain active. Please select at least one workspace.",
    EditCustomerFromContract: "Edit",
    EditCustomerFromContractTitle: "Edit customer",
    EditCustomerFromContractAria: "Edit customer",
    OptionMenuHeader: "Options",
    CustomerPageNoContractsTryThis: "Try these suggestions:",
    CustomerPageNoContractsText1: "Confirm you are setup as a TAM on a Clarify contract schedule",
    CustomerPageNoContractsText2: "Confirm the Clarify contract is active",
    CustomerPageNoContractsText3: "If the statements above are correct, please email ",
    ContractsPageHeader: "Manage contracts for ",
    ContractsPageSubHeader: "Contracts define the customer, providing entitlements for workspaces. Once a contract has been added, entitlements can then be configured for individual workspaces. Add/remove contracts to change the customer definition.",
    ContractNameTableHeader: "Contract Name",
    ContractNumberTableHeader: "Contract Number",
    ContractStartDateTableHeader: "Start Date",
    ContractEndDateTableHeader: "End Date",
    CustomersEditPageHeader: "Editing ",
    CustomersEditPageSubHeader: "When editing a customer, select the contracts containing the customer’s entitlements. Create the customer, and then you can define independent workspaces where data will be restricted to the members of that workspace. If a new contract becomes available, you can return to this page and add the agreement at that time.",
    WorkspacePageHeader: "Creating a workspace for ",
    WorkspacePageEditHeader: "Editing workspace ",
    WorkspaceSubHeader: "Create and manage a workspace for your customer. You can select which contracts and schedules to associate with this workspace. Give it a unique name that will be recognizable to both you and the customer, and invite users to join as workspace administrators.",
    WorkspaceStepOneTitle: "Step 1: Select contract schedules",
    WorkspaceStepOneInfo: "Please select the schedules to be associated with this workspace. All selected entitlements will be displayed inside the workspace.",
    ContractScheduleStepButtonAriaLabel: "Contract schedules step button",
    NameWorkspaceTitle: "Step 2: Name customer workspace",
    NameWorkspaceDescription: "Set a customer friendly workspace name to be displayed inside the Services Hub",
    AddWorkspaceAdminsTitle: "Step 3: Add workspace administrators (optional)",
    AddWorkspaceAdminsDescription: "Add administrators for the workspace. Workspace administrators can add or remove workspace users and manage user permisssions.",  
    NameWorkspaceInputLabel: "Workspace name",
    AddUsersEmailInputLabel: "Email address",
    AddUsersLanguageLabel: "Language",
    AddUsersEmailPlaceholder: "Enter a valid email address",
    NameWorkspaceStepButtonAriaLabel: "Name workspace step button",
    RemoveWorkspaceAdminTitle: "Remove workspace administrator",
    RemoveWorkspaceAdminAria: "Remove workspace administrator button",
    RemoveGlobalAdminTitle: "Remove global administrator",
    RemoveGlobalAdminAria: "Remove global administrator button",
    WorkspaceNameBoxTitle: "Workspace name",
    AddWorkspaceAdminEmailAria: "Add workspace admin email button",
    AddWorkspaceAdminEmailTitle: "Add workspace admin email",
    AddGlobalAdminEmailAria: "Add global admin email button",
    AddGlobalAdminEmailTitle: "Add global admin email",
    ManageWorkspaceHeader: "Manage workspaces for ",
    ManageWorkspaceSubHeader: "Create and manage workspaces for your customer to provide access to their digital entitlements.",
    ManageWorkspaces: "Manage workspaces",
    AddWorkspaceAdminsEmailPlaceholder: "Enter and add a valid email address",
    WorkspaceNamePlaceholder: "Workspace",
    UnitName: "Unit Name",
    Quantity: "Quantity",
    Type: "Type",
    CreateWorkspaceButton: "Create workspace",
    CreateWorkspaceButtonAria: "Create workspace button",
    EditWorkspaceButton: "Edit workspace",
    EditWorkspaceButtonAria: "Edit workspace button",
    WorkspaceVerifyStep: "Step 4: Verify information",
    WorkspaceVerifyInfo: "Please verify that all of the information is correct before continuing. If you would like to make any changes, scroll up and select the edit button for that section.",
    WorkspaceVerifyCustomer: "Customer:",
    WorkspaceVerifySchedules: "Schedules:",
    WorkspaceVerifyWorkspace: "Workspace:",
    WorkspaceVerifyAdmins: "Administrators:",
    CreateCustomerWorkspaceTitle: "Your workspace has been created",
    CreateCustomerWorkspaceMessage: "Congratulations! Your workspace has now been set up. If you've added an administrator to this workspace they will be notified of their new role via email. Visit the Manage Users page to invite the first members.",
    CreateAWorkspace: "Create a workspace",
    CreateAWorkspaceArialLabel: "Create a workspace button",
    CreateCustomerWorkspaceBtnText: "Go to Contract Details",
    WorkspaceNameTableHeader: "Workspace name",
    WorkspaceAdministratorCountTableHeader: "Administrators",
    WorkspaceMemberCountTableHeader: "Members",
    WorkspaceAdministratorCountModalAttribute: "Administrators:",
    WorkspaceMemberCountModalAttribute: "Members:",
    AddWorkspaceTitle: "Add a workspace",
    EditWorkspaceTitle: "Edit a workspace",
    UpdateCustomerWorkspaceTitle: "Updating your workspace",
    UpdateCustomerWorkspaceMessage: "The following schedules and administrators will be removed from this workspace. Cancel to edit, or save changes to continue.",
    UpdateCustomerWorkspaceBtnText: "Save changes",
    GlobalAdministrators: "Global administrators",
    GlobalAdminsHeader: "Global administrators for",
    GlobalAdminsSubHeader: "Global administrators can manage workspace settings for your organization. You can make other users global administrators by adding them below.",
    AddGlobalAdminButtonTitle: "Add global admin",
    RemoveGlobalAdminButtonTitle: "Remove global admin",
    AddGlobalAdmins: "Add Global Administrators",
    AddGlobalAdminsArialLabel: "Add Global Administrators button",
    GlobalAdminDisplayNameTableHeader: "Display name",
    GlobalAdminEmailTableHeader: "Email Address",
    GlobalAdminStatusTableHeader: "Status",
    GlobalAdminStaticText: "Global administrator",
    UnregisteredStaticText: "Unregistered",
    PendingStaticText: "Pending",
    ResendInviteButtonTxt: "Resend Invite",
    PendingModalMessage: "You invited {0} to be a global administrator for {1} on {2}. Their status is still pending because they have yet to accept your invitation. You can either contact them personally or resend the initial invite.",
    RemoveGlobalAdminModalTitle: "Remove global administrator",
    RemoveGlobalAdminModalConfirmation: "You are about to remove one or more global administrators from {0}, are you sure you want to do this?",
    GlobalAdminNameFormat: "{0} {1} ({2})", // Replacement string for a name with email; 0=First Name 1=Last Name 2=Email
    AddGlobalAdminModalMessage: "Enter a user's email address to make them a global administrator for your organization.",
    WorkspaceEdit: "Edit workspace",
    WorkspaceAdd: "Add workspace",
    ResendInviteMailConfirmationModalMessage: "An invitation email was resent to {0}",
    ResendInviteMailProcessModalMessage: "Resending an invitation email to {0}",
    ResendInviteMailConfirmationModalTitle: "",
    ResendInviteMailConfirmationModalOk: "OK"
}
export type ICustomerResx = typeof CustomerResx;