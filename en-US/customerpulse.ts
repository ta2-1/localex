﻿export var CustomerPulseResx = {
    O365SurveyTitle: "O365 Customer Pulse",
    O365DataStorageDescription: "We’re listening! Would you like to help influence improvements and new features in the software that powers your business? The Customer Pulse program provides an opportunity for you to give feedback about the Microsoft products that are important to you. Your feedback goes directly to our product engineering teams. Please take the short survey to let us know what you like – or what you’d like to see changed.",
    O365SurveyDescription: "Help shape Office 365! We need your feedback to ensure that Office 365’s services are easy to use and give you what your business needs. We would really like to hear about your experience and ideas on a regular basis, and it is so easy.",
    O365SurveyOverviewLinkText: "Check out the Customer Pulse Overview for more details.", // "Customer Pulse" should not be localized
    TakeSurvey: "Provide feedback",
    RetakeSurvey: "Provide feedback",
    Duration: "Duration 10 minutes",
    Responses: "Responses",
    Response: "Response",
    Tenant: "tenant",
    SelectO365Tenant: "Select Office 365 Tenant",
    Completed: "Completed",
    Participant: "Participant",
    Participants: "Participants",
    BackToSurveyList: "Back to Survey List",
    Colon: ":",
    AverageWithColon: "Average:",
    UpdateDocumentText: "View our <a id='customer-pulse-update-document' href='https://aka.ms/O365QuarterlyUpdateDocument' target='_blank'>Quarterly Update document</a> to see key improvements that the Office 365 team has made in response to your feedback.",
    OnAScaleOf: "On a scale of",
    To: "to",
    CustomerPulse: "Customer Pulse",
    CustomerPulseResponses: "Customer Pulse Responses"
}
export type ICustomerPulseResx = typeof CustomerPulseResx;