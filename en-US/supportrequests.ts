﻿export var SupportRequestsResx = {
    OpenCases: "Open Cases",
    ClosedCases: "Closed Cases",
    SorryUnauthorized: "Sorry, but your account is not authorized to see support cases on this page.",
    RequestAccess: "If you need access to support cases:",
    RequestAccessSuggestion1: "Ask your CSM or TAM to configure your account as a support contact.",
    NoOpenCase: "There are no open cases available for you to view.",
    NoClosedCase: "There are no closed cases available for you to view.",
    DataDelayDisclaimer: "The data on this page is not updated in real time and may be delayed up to 24 hours."
}
export type ISupportRequestsResx = typeof SupportRequestsResx;