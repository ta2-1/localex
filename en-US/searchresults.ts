export var SearchResultsResx = {
    SearchResults: "Search services hub",
    Search: "Search",
    Preview: "The Search capability on Services Hub is still in preview. We would love to hear about your views on the functionality and relevance. Please share your feedback on <a id='search-result-user-voice-link' target='_blank' href='https://serviceshub.uservoice.com/'>user voice</a>.",
    SearchFor: "Search for...",
    CurrentQueryLabel: "Showing results for : ",
    NoResults: "We could not find any results for this query. We are working on enhancing our search capabilities and would really appreciate your feedback on User voice."
}
export type ISearchResultsResx = typeof SearchResultsResx;
