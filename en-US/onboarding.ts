﻿export var OnboardingResx = {
    Onboarding: "Onboarding",
    Step: "Step",

    //step 1
    TellUsAboutYourself: "Tell us about yourself",
    Step1Info: "This information helps us connect you to the latest Microsoft services, education, tools, and more!",
    FirstName: "First name",
    LastName: "Last name",
    EmailAddress: "Email address",
    ITDecisionMaker: "IT decision maker",
    ITImplementer: "IT implementer",
    Neither: "Neither",
    YourFunction: "Your function",

    //step 2
    AddYourTechnicalInterests: "Add your technical interests",
    Step2Info: "This information helps us provide you the most relevant content.",
    Interests: "Interests",
    TechnicalInterests: "technical interests",

    //step 3
    ChooseYourFocusAreas: "Select your focus areas",
    FocusAreaTitle: "This information helps us provide you the most relevant content.",
    FocusAreas: "Focus areas",
    AvailabilityAndBusinessContinuity: "Availability and business continuity",
    ChangeConfiguration: "Change and configuration management",
    OperationsMonitoring: "Operations and monitoring",
    PerformanceScalability: "Performance and scalability",
    SecurityCompliance: "Security and compliance",
    MigrationDeployement: "Upgrade, migration, and deployment",
    BusinessItAlignment: "Business/IT alignment",

    //step 4
    InviteColleagues: "Invite colleagues",
    EmailInstruction: "Please enter individual email addresses and not team aliases.",
    EmailValidationMessage: "One or more email addresses are not valid. Please try again.",
    Invite: "Invite",
    EnterEmailAddress: "Enter email address",
    EmailVallidationStep1Message: "Email address is not valid, please try again.",

    //step 5
    WelcomeToTheMicrosoftServicesHub: "Welcome to the Microsoft Services Hub",
    Congratulations: "Congratulations!",
    Step5Description: "Start exploring how the Microsoft Services Hub can help you achieve more, stay connected, and save time! ",
    Step5ListTitle: "With the Microsoft Services Hub, you can:",
    GetUnlimitedUse: "Get unlimited use of on-demand education, including virtual labs and online courses.",
    StayConnected: "Stay connected and current with Services Hub Update O365 Center.",
    GetCrucialInformation: "Get crucial information about your support cases, contracts, and contracts consumption.",
    PreventManageRisks: "Prevent and manage risks with on-demand premier assessments.",
    TermsAndConditions: "Terms and Conditions",
    TermsAndConditionsAgreement: "I want to participate in Microsoft Services Hub research. I agree to these terms and conditions.",
    Close: "Close",
    OptNewsletter: "I want to receive email communications about the latest news and updates from Services Hub.",

    ChooseYourRoleAndTechnicalInterest: "Define your Preferences",
    Expertise: "Expertise",

    //PSE step 1
    Step1Title: "Welcome to Services Hub’s new support experience!",
    Step1Information: "This new experience has been created to help streamline your organization's Microsoft support experience. You can:",
    Step1Item1: "Open and manage support requests",
    Step1Item2: "Update support request details",
    Step1Item3: "Share support requests with your peers",
    Step1Item4: "View cases across multiple workspaces",

    //PSE step 2
    Step2Title: "Please verify your information",
    Step2Information: "Help us customize your experience by verifying your information below. This will help you submit support requests more quickly and provide you with faster support.",

    //step 3
    Step3Title: "A better way to manage your support requests",
    Step3Information: "On the Manage Requests page, you’ll see the support requests you create and any that have been shared with you.",
    Step3ItemTitle: "Additionally, on this page you can:",
    Step3Item1: "View details of the support request",
    Step3Item2: "Create a new support request",
    Step3Item3: "View who opened the support request",

    //step 4
    Step4Title: "Understanding workspaces",
    Step4Information: "Each support request is associated to a Services Hub workspace.  Members will only see support requests associated to that workspace.",
    Step4Information2: "Members who have access to multiple workspaces can easily navigate between them to view their support requests.",

    //step 5
    Step5Title: "You’re all set!",
    Step5Information: "Thank you for telling us about you!",
    Step5Information2: "Now, you are ready to get started on Services Hub. You can always change your information in your Profile.",

    step3Image: "Screenshot of manage support requests page.",
    step4Image: "Screenshot of workspaces menu."
}

export type IOnboardingResx = typeof OnboardingResx;