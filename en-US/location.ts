﻿export var LocationResx = {
    CityOrPostalCode: "City or Postal Code",
    Search: "Search"
}
export type ILocationResx = typeof LocationResx;