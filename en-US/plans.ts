﻿export var PlansResx = {
    Plans: "Plans",
    Tasks: "Tasks",
    Edit: "Edit",
    Remove: "Remove",
    RemovePlan: "Remove plan",
    Owner: "Owner",
    TaskOwner: "Task owner",
    TaskOwnerDescription: "Task owners are expected to oversee completion of a task and have access to edit the tasks in a plan.",
    TargetDate: "Target date",
    TasksCompleted: "tasks completed",
    Of: "of",
    InputTaskPlaceHolder: "Enter text to select a service from the catalog or create a custom task",
    AddTask: "Add task",
    ChoosePlanType: "Choose a plan type",
    ChoosePlanTypeFlow: "Create a new plan or choose an existing template",
    PlanTypeNewChoiceLabel: "Create a new plan",
    PlanTypeTemplateChoiceLabel: "Choose an existing template",
    Duration: "Duration",
    Contact: "Contact",
    StartDate: "Start date",
    EndDate: "End date",
    EditDetails: "Edit details",
    RemoveTask: "Remove task",
    RemoveTasks: "Remove tasks",
    CloneTask: "Clone task",
    Notes: "Notes",
    BackToPlan: "Back to plan",
    TaskDetails: "Task details",
    CompleteTask: "Complete task",
    ReactivateTask: "Reactivate task",
    EditTaskDescription: "Set your task to track the activities that will help you achieve your goals. You can return anytime to update your tasks.",
    Name: "Name",
    Description: "Description",
    Cancel: "Cancel",
    Next: "Next",
    Save: "Save",
    Days: "Days",
    WhoIsTheContact: "Who is the contact for this task?",
    SelectOwnerFromList: "Please select an owner",
    SelectFromListPlaceholder: "Select from list", 
    PlanDetails: "Plan details",
    EditPlanDescription: "Update your plan to track important activities. You can return anytime to update your plan.",
    NewPlanDescription: "Create a new plan to track important activities. You can return anytime to update your plan.",
    ChoosePlanTypeDescription: "Assessment remediation plans allow you to bulk add all recommendations from all the asessments you configured and surverys you have taken. Choose custom if you do not wish to connect to an Assessment remediation plan",
    PlanName: "Plan name",
    AssessmentPlanTemplate: "Plan template",
    AssessmentPlanDetails: "Your plan",
    AddAssessmentTask: "Add Recommendations",
    AddAssessmentTasksDescription: "Add recommendations from assessment data",
    SelectTemplatePrompt: "Please select a template",
    WhichAssessment: "Which assessment do you want to create the plan from?",
    NoTasksNotice: "This plan currently has no tasks",
    NoTasksInstruction: "To add tasks to your plan, please find the task you would like to add or create a custom task by using the form field above.",
    CloneTaskDescription: "Cloned task owner",
    WhoIsTheOwner: "Who is the owner for this plan?",
    RemoveTaskConfirmation: "Please confirm you want to permanently remove this task from the plan.",
    RemovePlanConfirmation: "Please confirm you want to permanently remove this plan.",
    Pending: "Pending",
    InProgress: "In progress",
    Completed: "Completed",
    Expired: "Expired",
    Scheduled: "Scheduled",
    PendingSchedulingAlert: "Pending scheduling. Please contact the owner to schedule this service to ensure it becomes part of your plan.",
    PendingSchedulingAlertOwner: "Pending scheduling. Please schedule this service to ensure it becomes part of your plan.",
    Status: "Status",
    NoTasksDefined: "No tasks defined",
    UpdatedDaysAgo: "Updated {0} days ago",
    UpdatedToday: "Updated today",
    UpdatedYesterday: "Updated yesterday",
    NotDefined: "Not defined",
    MyPlans: "My plans",
    PlansFor: "Plans for {0}",
    CompletedPlans: "Completed plans",
    IndividualPlans: "Plans for individuals",
    CreateNewPlan: "Create a new plan",
    NoTasksTimelineMessage: "Add tasks to your plan to visualize them in the timeline",
    NotePlaceHolder: "Type your note here...",
    AddNote: "Add note",
    CharacterLimit: "{0} of {1} (character limit)",
    RemoveNote: "Remove Note",
    RemoveNoteConfirmation: "Are you sure you want to remove this note from your note list?",
    NoteFrom: "Note from",
    NoteDeletedBy: "Note deleted by",
    Members: "Members",
    MembersDescription: "Add members to your plan to choose who can view and participate. Turning off the member list makes the plan accessible to everyone inside your workspace.",
    DisplayUserFormatString: "{0} {1} ({2})", // this format string is used to display {0}=First name {1}=Last name {2}=Email. If any other language displays the Last name before the first name, the localized string would be "{1} {0} ({2})" instead
    ViewMode: "ViewMode",
    EditMode: "EditMode",
    AddParticipant: "Add Participant",
    AddMemberPlaceHolder: "Enter or select a name",
    RemoveMemberTitle: "Remove Member",
    ViewRoleToggleTitle: "View",
    EditRoleToggleTitle: "Edit",
    RemoveMemberConfirmation: "Are you sure you want to remove this user from the plan and all associated tasks?",
    Confirmed: "Confirmed",
    ViewDetails: "View details",
    Watch: "Watch",
    SelectIndividualPlaceHolder: "Select a user to view their individual plans",
    DownloadReport: "Download Report",
    TypeWithColon: "Type:",
    AnnotatedWithColon: "Annotated:",
    OwnerWithColon: "Owner:",
    StatusWithColon: "Status:",
    Day: "Day",    
    Filter: "Filter",
    SelectAll: "Select all",
    Actions: "Actions",
    AnnotateTask: "Annotate task",
    EditDueDate: "Edit due date",
    EditOwner: "Edit owner",
    ViewInOMS: "View in OMS",
    Yes: "Yes",
    No: "No",
    Custom: "Custom",
    EditOwnerDescription: "Bulk editing an owner will add the same owner to each recommendation selected within your plans task list. please select an owner below.",
    Update: "Update",
    AnnotateTaskDescription: "An annotation will be added to each recommendation selected from your plans tasks list. Please enter your message in the field below",
    Add: "Add",
    Annotation: "Annotation",
    AnnotateTasks: "Annotate tasks",
    EditDueDateMessage: "Editing the due date will add the same end date to each recommendation selected from your plans task list. Please select your end date below.",
    WhenIsEndDateForTask: "Who is the owner for these tasks?",
    CompleteTasksActionMessage: "You are about to mark {0} tasks as complete. Are you sure?",
    RemoveTasksActionMessage: "You are about to remove {0} tasks from your plan. Are you sure?",
    CompleteTasksActionTitle: "Complete tasks"
}
export type IPlansResx = typeof PlansResx;