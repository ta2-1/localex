export var PublicResx = {
    AppName: "Volta",
    HeroImageCaption1: "Microsoft Services Hub",
    HeroImageCaption2: "Simple. Powerful. Adaptive.",
    HeroImageCaption3: "Sign in",
    SignUp: "Sign up",
    LogIn: "Log in",    
    Connect: "Connect.",
    SaveTime: "Save time.",
    AchieveMore: "Achieve more.",
    SignIn: "SIGN IN",
    NeedHelpSigning: "Need help signing in?",
    VisitResourceCenter: "Visit our resource center to learn more"
}
export type IPublicResx = typeof PublicResx;