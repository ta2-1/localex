﻿export var ContractResx = {
    ContractDetails: "Contract Details",
    Type: "Type",
    Level1: "Level 1",
    Level2: "Level 2",
    Level3: "Level 3",
    Level50: "Level 50",
    /*START - KEEP THESE STRINGS ENGLISH ONLY*/
    NewLevel1: "Microsoft Unified Support Core",
    NewLevel2: "Microsoft Unified Support Advanced",
    NewLevel3: "Microsoft Unified Support Performance",
    NewLevel50: "Microsoft Premier",
    /*END - KEEP THESE STRINGS ENGLISH ONLY*/
    SevAndSituation: "Severity and Situation",
    ResponseTime: "Response Time",
    Critical: "Critical",
    Standard: "Standard",
    CriticalImpact1: "Loss of a core business process:",
    CriticalImpact2: "work cannot reasonably continue",
    StandardImpact1: "Moderate loss or degredation of services:",
    StandardImpact2: "work can continue in an impaired manner",
    Hours: "Hours",
    Hour: "Hour",
    Minutes: "Minutes",

    ProactiveServices: "Available Services",
    Assessment: "Assessment services",
    SelfService: "On-demand Assessment - Self-serve",
    SelfServiceDetail: "Access to a self-serve, automated assessment platform you can use to assess your Microsoft technology implementation with data collected locally for you to analyze.",
    SelfServiceLinkText: "View On-demand Assessments",
    Remote: "On-demand Assessment - Remote Engineer Support",
    RemoteDetail: "In conjunction with the On-demand Assessment use, remote engineering support (up to one day) can be purchased to assist with analyzing the data and prioritizing remediation recommendations.",
    Onsite: "On-demand Assessment - Onsite Engineer Support",
    OnsiteDetails: "In conjunction with the On-demand Assessment use, onsite support (up to two days) can be purchased to assist with analyzing the data and prioritizing remediation recommendations.",
    RAPAsAService: "Risk and Health Assessment Program as a Service (RAP as a Service)",
    RAPAsAServiceDetails: "An automated assessment of your Microsoft technology implementation, with data collected remotely. The gathered data is analyzed by Microsoft to create a findings report containing remediation recommendations.",
    ArchitecturalServices: "Architectural services",
    ArchitecturalServicesDetails: "An evaluation of your online services adoption goals which provides guidance, planning, and remediation to establish better alignment of your teams and environment with architecture best practices.",

    Education: "Education services",
    OnDemandSubscription: "On-demand Subscription",
    OnDemandSubscriptionDetails: "Access to a collection of online training materials from a workshop library developed by Microsoft support engineers.",
    OnDemandSubscriptionLinkText: "View On-demand Education",
    Webcasts: "Webcasts",
    WebcastsDetails: "Access to Microsoft-hosted educational sessions, available on a wide selection of support and Microsoft technology topics, delivered over the Internet.",

    OperationServiceTypes: "Operation services",
    CriticalSecuritySupportAdvice: "Critical security support advice",
    CriticalSecuritySupportAdviceDetails: "Notification of critical Microsoft Security Bulletins. If you have a Designated or Dedicated SDM, your SDM will help you assess the effect of this information on your IT infrastructure.",

    AdoptionSupportServices: "Adoption Support services",
    AdoptionScenarioWorkshop: "Adoption Scenario Workshop",
    AdoptionScenarioWorkshopDetails: "A two-day remote or onsite workshop showcasing best practices for increasing adoption for a specific online services workload.  Workshops are available on an open per-attendee basis or as a dedicated closed delivery to your organization, as specified on your Work Order. Workshops cannot be recorded without express written permission from Microsoft.",

    DeveloperFocusedServiceTypes: "Developer focused services",
    ServicesInsightsForDeveloper: "Services Insights For Developers",
    ServicesInsightsForDeveloperDetails: "An annual assessment of your application development practices to help customers with recommended practice guidance for developing applications and solutions on Microsoft platforms.",
    
    ReactiveServiceTypes: "Reactive services",
    AdvisoryServices: "Advisory Services",
    AdvisoryServicesDetails: "Phone-based support on short-term (typically six hours or less) and unplanned issues for IT Professionals and Developers. Advisory Services may include advice, guidance, and knowledge transfer intended to help you implement Microsoft technologies in ways that avoid common support issues and that can decrease the likelihood of system outages.",
    ProblemResolutionSupport: "Problem Resolution Support (PRS)",
    ProblemResolutionSupportDetails: "This assistance for problems with specific symptoms encountered while using Microsoft products includes troubleshooting a specific problem, error message or functionality that is not working as intended for Microsoft products. Severity definitions, the Microsoft estimated initial response times, and submission requirements are detailed in the following incident response table.",
    EscalationManagement: "Escalation Management",
    EscalationManagementDetailsLevel1: "Escalation provides oversight of support incidents to drive timely resolution and a high quality of support delivery. For critical and non-critical incidents, the service is available by customer request during business hours into pooled service delivery resources. These resources may also provide escalation updates, when requested.",
    EscalationManagementDetailsLevel2: "Escalation provides oversight of support incidents to drive timely resolution and a high quality of support delivery. For non-critical incidents, the service is available by customer request during business hours to the pooled service delivery resource who can also provide escalation updates when requested. In critical business impact severity situations, a special escalation process is automatically executed after 4 hours if the normal business function is not recovered after the issue has been assigned a severity level.  A Critical Situation Manager will then be assigned to the issue, and is responsible for ensuring continued technical progress on the issue and providing you with status updates and an action plan.",
    EscalationManagementDetailsLevel3: "Escalation provides oversight of support incidents to drive timely resolution and a high quality of support delivery. For non-critical incidents, the service is available by customer request during business hours to the pooled service delivery resource who can also provide escalation updates when requested. In critical business impact severity situations, a special escalation process is automatically executed immediately if the normal business function is not recovered after the issue has been assigned a severity level.  A Critical Situation Manager will then be assigned to the issue, and is responsible for ensuring continued technical progress on the issue and providing you with status updates and an action plan.",
    
    ServiceDeliveryManagement: "Service delivery management",
    LifecycleAwareness: "Microsoft Product/Online Services lifecycle awareness",
    LifecycleAwarenessDetails: "Content provided to help you plan for and respond to, important Microsoft technology-related security notices and Microsoft product roadmap or lifecycle changes. For customers who have online services, you will also receive information on service upgrades and planned outages for your subscribed online services.",
    ServiceIntroduction: "Service introduction",
    ServiceIntroductionDetailsLevel1: "An introductory overview of available services including an explanation of how to select and plan Proactive services",
    ServiceIntroductionDetailsLevel2And3: "An initial overview of available services with your services delivery manager including an explanation of how to select and plan Proactive services.",
    ServiceRecommendation: "Service recommendations",
    ServiceRecommendationDetailsLevel1: "At your request, a service delivery resource will discuss available support options for your environment.",
    ServiceRecommendationDetailsLevel2: "Recommendations prepared by your designated services delivery manager and your support technical advisor as part of your services delivery planning. Recommendations may also be made after assessments or after significant events to discuss available services to help support your business and technology objectives.",
    ServiceRecommendationDetailsLevel3: "Recommendations prepared by your designated services delivery manager as part of your services delivery planning. Recommendations may also be made after assessments or after significant events to discuss available services to help support your business and technology objectives.",
    ServiceDeliveryPlan: "Service Delivery Plan (SDP)",
    ServiceDeliveryPlanDetails: "Your service delivery manager will work with you to develop a plan for how and when support services you have purchased are delivered. We will monitor and make ongoing adjustments to your SDP based on your needs.",
    ServiceOutageManagement: "Service outage management",
    ServiceOutageManagementDetails: "Oversight of incidents that significantly degrade or interrupt access to Microsoft online services is provided. This may include a summary of the cause of the incident, the service level impact after incidents or preparation and review of an incident response plan.",
    ServiceReviews: "Service reviews",
    ServiceReviewsDetails: "Microsoft can review past period’s services, report to you on what has been delivered, review your feedback, and discuss any actions or adjustments which may be required. These reviews, delivered once every two months virtually or onsite, may consist of standard status reports status meetings. If you have online services, your service reviews will include status of service requests and service incidents, as well as, uptime information.",
    CrisisManagement: "Crisis management",
    CrisisManagementDetails: "Around-the-clock issue ownership and communication to you from Critical Situation Managers during critical business impact situations.",
    InitialAssessment: "Intial Assessment",
    InitialAssessmentDetails: "A discovery assessment for identifying service needs to support your people, processes and technology. For customers who have online services, we can arrange to analyze the onpremises infrastructure required to connect users to the online service. In addition, the service delivery resource will work with your staff to document risks that may impact your connectivity and suggest potential mitigations.",
    ServiceReviewReports: "Service review reports",
    ServiceReviewReportsDetails: "At your request, a service delivery resource can review the past period’s services, report to you on what has been delivered, review your feedback and discuss any actions or adjustments which may be required.",

    BenefitsFootnote: "Consult your Work Order or Technical Account Manager for quantities included as part of your support level. Additional services may also be purchased.",
    ContactTam: "Contact TAM to Upgrade",
    ReactiveSupport: "Reactive Support",
    ScheduleNumberTitle: "Schedule:",
    ContractNumberTitle: "Contract:",
    TAMEmail: "Technical Account Manager Email:",
    TAM: "Technical Account Manager",
    ScheduleStartDate: "Schedule Start:",
    ScheduleEndDate: "Schedule End:",
    SupportContacts: "Support Contacts",
    CSM: "Services Admin", // "Services Admin" should not be localized
    ContractSupportExhibitTitle: "Support Contract Exhibit:",
    DownloadServicesDescription: "Support & Consulting Services Description",
    ScheduleDates: "Schedule Dates:",
    CustomerAction_Cancel: "Cancel",
    CustomerAction_MoreInformation: "More Information",
    CustomerAction_Postpone: "Postpone",
    CustomerAction_StartScheduling: "Start Scheduling",
    PreferredDate: "Preferred date",
    AccessIdNotAvailable: "Not Available",
    SupportContractDetails: "Full Support Contract Details",
    IncludedServices: "Included Benefits for Level",
    CannotViewContractDetailsHeading: "Under construction",
    CannotViewContractDetailsBody: "This page is currently available to select people only. We will explore opening it up to more people as we work to enhance the experience. Keep checking back!"
}
export type IContractResx = typeof ContractResx;