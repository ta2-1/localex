﻿export var SupportCaseContactResx = {
    Title: "For critical issues",
    Warning: "If your issue is critical and requires immediate attention, please call your regional office.",
    CountryRegion: "Select a country or region",
    PreferredPhone: "Preferred",
    AlternatePhone: "Alternate",
    AccessId: "Your access ID",
    AccessIdToolTip: "You will be asked to provide your access ID when calling Microsoft support. It helps confirm your identity and the contract to associate your request against.",
    AlternateContacts: "Alternative contacts",
    PremierPartnerAdvantage: "Premier and Partner Advantage",
    PremierPartnerAdvantagePhone: "1-800-936-3500",
    MicrosoftPartnerNetwork: "Microsoft Partner Network",
    MicrosoftPartnerNetworkPhone: "1-800-936-2200",
    MicrosoftSalesInformation: "Microsoft Sales and Information",
    MicrosoftSalesInformationPhone: "1-800-936-3500",
    MicrosoftSalesInformationWorkingHours: "United States hours: 6:00am-6:00pm PST Monday-Friday, excluding holidays",
    MicrosoftSalesTextTelephone: "Text telephone (TDD/TTY)",
    MicrosoftSalesTextTelephoneTollFree: "1-800-892-5234",
    MicrosoftSalesTextTelephoneLocal: "1-425-635-4948",
    InvalidEntry: "Invalid Entry",
}

export type ISupportCaseContactResx = typeof SupportCaseContactResx;