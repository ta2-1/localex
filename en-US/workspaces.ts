﻿export var WorkspacesResx = {
    WorkspaceId: "Workspace Id",
    CreatedDate: "Created Date",
    LastUpdatedDate: "Last Updated Date",
    WorkspaceName: "Workspace Name",
    CustomerAccountId: "TPID",
    ContractId: "Contract Id",
    ScheduleId: "Schedule Id",
    Level: "Level",
    Workspaces: "Workspaces",
    FilterResults: "Filter Results",
    NoResultFound: "Sorry, no matches found for your search. Please try again.",
    CompanyName: "Company Name",
    ContractNumber: "Contract Number",
    ExternalId: "External Id",
    TPID: "TPID",
    IsMember: "Is Member",
    MsaException: "MSA Exception",
    MsaExceptionAsteriskDefinition: ": Workspaces with MSA Exception",
    MsaExceptionAsteriskTooltip: "Workspaces with MSA Exception",
}

export type IWorkspacesResx = typeof WorkspacesResx;