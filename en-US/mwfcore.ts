﻿export var MwfCoreResx = {
    More: "More",
    Less: "Less"
}
export type IMwfCoreResx = typeof MwfCoreResx;