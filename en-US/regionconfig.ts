﻿export var RegionConfigResx = {
    Australia: {
        Name: "Australia",
        PreferredPhoneNumber: "1-800-283-173",
        AltPhoneNumber: "+61 280318536 (local charges apply)",
        Availability: "Monday to Friday, 8am to 5pm local time"
    },
    Canada: {
        Name: "Canada",
        PreferredPhoneNumber: "1-855-425-8237",
        Availability: "Monday to Friday, 7am to 7pm US Central Time"
    },
    France: {
        Name: "France",
        PreferredPhoneNumber: "08 05 54 06 42",
        AltPhoneNumber: "+33 157324392 (local charges apply)",
        Availability: "Monday to Friday, 8am to 5pm local time"
    },
    Germany: {
        Name: "Germany",
        PreferredPhoneNumber: "0800-7234089",
        AltPhoneNumber: "+49 69999915794 (local charges apply)",
        Availability: "Monday to Friday, 8am to 5pm local time"
    },
    Mexico: {
        Name: "Mexico",
        PreferredPhoneNumber: "1-800-563-0672",
        AltPhoneNumber: "+52 55 525 89115 (local charges apply)",
        Availability: "Monday to Friday, 8am to 5pm local time"
    },
    Sweden: {
        Name: "Sweden",
        PreferredPhoneNumber: "02 0 160 59 05",
        AltPhoneNumber: "+46  8 5 176 19 72 (local charges apply)",
        Availability: "Monday to Friday, 8am to 5pm local time"
    },
    UnitedKingdom: {
        Name: "United Kingdom",
        PreferredPhoneNumber: "0800-731-3004",
        AltPhoneNumber: "+44 2034506463 (local charges apply)",
        Availability: "Monday to Friday, 8am to 5pm local time"
    },
    UnitedStates: {
        Name: "United States",
        PreferredPhoneNumber: "1-855-425-8237",
        Availability: "Monday to Friday, 7am to 7pm US Central Time"
    }
}

export type IRegionConfigResx = typeof RegionConfigResx;