﻿export var UsersResx = {
    Name: "Name",
    EmailAddress: "Email Address",
    Address: "Address",
    Status: "Status",
    Invite: "Invite",
    SendInvite: " (Invite) ",
    EmailToInvite: "Enter one or more email addresses (comma delimited)",
    InvalidEmail: "One or more email address entered is invalid. Please enter valid email addresses separated by commas.",
    RevokeRegistrationLinkText: "Remove",
    RemoveUserLinkText: "Remove",
    ResendInviteEmail: " (Resend)",
    InviteSubject: "Please register for Microsoft Services Hub",
    InviteBody: "Click this link to register",
    RemoveRegistrationModalTitle: "Are you sure you want to remove this invitation?",
    RemoveUserModalTitle: "Are you sure you want to remove this person from this workspace?",
    RemoveModalBody: "You will have to reinvite this person if you change your mind. ",
    RemoveRegistrationAndDisassociateModalTitle: "Are you sure you want to remove this invitation and all his/her support roles on the schedule?",
    RemoveUserAndDisassociateModalTitle: "Are you sure you want to remove this person from this workspace and all his/her support roles on the schedule?",
    RemoveAndDisassociateModalBody: "You will have to invite this person and add all his/her support roles on the schedule again if you change your mind. ",
    RemoveModalOk: "Remove",
    RemoveModalCancel: "Cancel",
    RemoveSupportContactModalTitle: "Are you sure you want to remove this user from all Services Hub support roles?",
    InviteUserModalTitle: "Are you sure you want to invite these users?",
    InviteUserModalBody: "Inviting users to this workspace grants them access to the same views and data that you see.",
    InviteUserModalMultipleUserCount: "Users",
    InviteUserModalSingleUserCount: "User",
    InviteUserModalOk: "Invite",
    Pending: "Invite sent",
    Registered: "Registered",
    EmailRequested: "Email Requested",
    EmailSent: "Email Sent",
    EmailFailed: "Email Failed",
    UnRegisteredSupportContactOrCSM: "Unregistered",
    SelectCulture: "Select culture",
    SupportContact: "Support Role",
    SupportContactHeader1: "Invite others from your team or organization to share your Services Hub workspace with them.",
    SupportContactHeader2: "Your Services Admin can manage who is allowed to contact Microsoft for support by designating support contacts.", // "Services Admin" should not be localized
    Locale: "Locale",
    SelectLocale: "Select locale",
    Ok: "Ok",
    InvitationMsg: "Invitation sent successfully.",
    SupportContactHelperText: "Support roles are able to open or view the status of Microsoft requests. The number of support roles is limited based on your Microsoft support agreement.",
    SupportContactHelperText_Classic: "Support roles are able to open or view the status of Microsoft requests.",
    CSMLabel: "Services Admin", // "Services Admin" should not be localized
    SupportContactInformation: "More information on support contact",
    AddOmsUser: "This would add the users to Azure Subscription and Log Analytics workspace as Contributor",
    OmsAccess: "Azure Log Analytics Access",
    OMSAccessInformation: "More information on Azure Log Analytics Access",
    OMSAccessHelperText: "Only the Azure Subscription owner can grant Log Analytics access. This gives the user permission as an Azure Contributor.",
    OMSAccessHelperTextSmall: "Only the Azure Subscription Owner can change Log Analytics access.",
    Email: "Email",
    SupportContactLimitUsed: "Used",
    SupportContactLimitAllowed: "Allowed",
    InviteUsers: "Invite Users",
    UnregisteredUser: "Unregistered User",
    AccountName: "Account Name",
    Office365PulseAccess: "Office 365 Pulse Access",
    O365PulseAccessHelperText: "Only the Services Admin and TAM can grant Office 365 Customer Pulse access. This gives the user permission to take the survey.", // "Services Admin" should not be localized
    IsMSAUserDescription: "* Denotes the user is using a Microsoft account",
    IsMSAUser: "*",
    MoreInfo: "More info",
    Close: "Close",
    AccountType: "Account Type",
    MicrosoftAccount: "Microsoft Account (Live ID)",
    WorkAccount: "Work Account",
    RegisteredOn: "Registered On",
    LastUpdated: "Last Updated",
    InvitedBy: "Invited By",
    InvitedOn: "Invited On",
    OpenModalRemoval: "Open modal to confirm the removal for",
    OpenModalMoreInfo: "Open modal to show more info for",
    RegistrationUrl: "Registration URL",
    InvitedEmail: "Invited Email",
    SystemGenerated: "System Generated",
    Colon: ":",
    ServicesAdmin: "Services Admin",
    Yes: "Yes",
    No: "No",
    HasAccess: "Has Access",
    SupportContactDownForMaintenanceDescription: "This feature is temporarily unavailable due to maintenance. It should be back online shortly.",
    AllowMicrosoftAccountsDescription: "Allow Microsoft Accounts",
    AllowMicrosoftAccountsTooltip1: "Microsoft Accounts, also known as Live ID or personal accounts, are not supported by Services Hub by default. Microsoft accounts are primarily intended for personal use and therefore lack organization-level controls. This means organizations cannot easily manage password policies or activate/deactivate them in a centralized manner.",
    AllowMicrosoftAccountsTooltip2: "Microsoft highly recommends all companies use work accounts when accessing business applications and services like Services Hub, as using Microsoft Accounts increases concerns around security and privacy for the customer’s organization.",
    AllowMicrosoftAccountsTooltip3: "For companies that are unable or unwilling to use work accounts, this setting can be changed to allow use of Microsoft Accounts.",
    AllowMicrosoftAccountsHelperPara1: "Microsoft Accounts, also known as Live ID or personal accounts, are not supported by Services Hub by default. Microsoft accounts are primarily intended for personal use and therefore lack organization-level controls. This means organizations cannot easily manage password policies or activate/deactivate them in a centralized manner.",
    AllowMicrosoftAccountsHelperPara2: "Microsoft highly recommends all companies use work accounts when accessing business applications and services like Services Hub, as using Microsoft Accounts increases concerns around security and privacy for the customer’s organization.",
    AllowMicrosoftAccountsHelperPara3: "For companies that are unable or unwilling to use work accounts, this setting can be changed to allow use of Microsoft Accounts.",
    AllowMicrosoftAccountsHelperPara4: "By changing this:",
    AllowMicrosoftAccountsHelperPara5: "You acknowledge that customer is aware of the implications of using Microsoft Accounts to register and sign-in to Services Hub.",
    AllowMicrosoftAccountsHelperPara6: "You acknowledge that discussions have been conducted around customer's plan, if any, on getting work accounts for their users.",
    RemoveMicrosoftAccountsHelperText: "By disabling this exception, any users registered using Microsoft Accounts will no longer be able to log on to this workspace.  However, their accounts will remain associated with the workspace.  To remove them completely, please remove their accounts from the manage access page.",
    AllowMicrosoftAccountsInfoText: "Microsoft accounts are primarily intended for personal use and therefore lack organization-level controls. Microsoft highly recommends all companies use work accounts when accessing Services Hub. For companies that are unable or unwilling to use work accounts, this setting can be changed to allow use of Microsoft Accounts.",
    Continue: "Continue"
}
export type IUsersResx = typeof UsersResx;