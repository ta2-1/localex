﻿export var RolesResx = {
    Roles: "Roles",
    IdHeader: "Id",
    NameHeader: "Name",
    ClaimsHeader: "Claims:",
    LastUpdatedHeader: "Last Updated",
    FilterResults: "Filter Results",
    SortById: "SortById",
    SortByName: "SortByName",
    SortByLastUpdated: "SortByLastUpdated",

    //role template friendly names for display: RoleName.Value
    GlobalAdministratorTemplateRole: "Global administrators",
    WorkspaceAdministratorTemplateRole: "Workspace administrators", 
    VoltaWorkspaceUserTemplateRole: "Workspace users", 
    ContractSupportUserTemplateRole: "Support contacts",
    ContractCSMUserTemplateRole: "Support administrators",
    ClassicWorkspaceAdministratorTemplateRole: "Workspace administrators", 

    // permission role template friendly names:
    ManageUsersPermissionTemplateRole: "Manage users",
    InviteUsersPermissionTemplateRole: "Invite users",
    CustomerPulsePermissionTemplateRole: "Customer pulse survey",
    SharedFilesPermissionTemplateRole: "Shared files",
    SupportCasePermissionTemplateRole: "View all support cases",
    TrainingPermissionTemplateRole: "Learning",
    AssessmentsPermissionTemplateRole: "On-demand assessments",
    ContractDetailsPermissionTemplateRole: "Contract details",
    CriticalAlertsPermissionTemplateRole: "Critical alerts",
    HealthPermissionTemplateRole: "Health",
    PlansPermissionTemplateRole: "Plans"
}

export type IRolesResx = typeof RolesResx;