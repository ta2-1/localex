﻿export var SettingsResx = {
    Settings: "Settings",
    IdHeader: "Id",
    NameHeader: "Name",
    ValueHeader: "Values",
    TypeHeader: "Type",
    IsActiveHeader: "IsActive",
    ServiceNameHeader: "Service Name",
    CreatedHeader: "Created",
    LastUpdatedHeader: "Last Updated",
    SettingName: "setting name",
    Edit: "Edit",
    Save: "Save",
    UpdateHeader: "Update",
    SettingValue: "setting value",
    SettingIsActive: "setting isactive",
    FilterResults: "Filter Results",
    SortByName: "Sort By Name",
    SortByServiceName: "SortByServiceName",
    SortByValue: "SortByValue",
    SortByType: "SortByType",
    SortByLastUpdated: "SortByLastUpdated"
}

export type ISettingsResx = typeof SettingsResx;