import { MainResx, IMainResx } from "./main"
import { HeaderResx, IHeaderResx } from "./header"
import {FooterResx, IFooterResx}  from "./footer"
import {ErrorResx, IErrorResx}  from "./error"
import {PublicResx, IPublicResx}  from "./public"
import {UpdateCenterResx, IUpdateCenterResx}  from "./updatecenter"

export var Resources = {
    Public: PublicResx,
    Header: HeaderResx,
    Footer: FooterResx,
    Error: ErrorResx,
    UpdateCenter: UpdateCenterResx,
    Main: MainResx
}

export type IResources = typeof Resources;