﻿export var HeaderResx = {
    SignIn: "Sign in",
    SignOut: "Sign out",
    Education: "Education",
    Assessments: "Assessments",
    ManageUsers: "Manage users",
    Insights: "Insights",
    Home: "Home",
    Workspaces: "Workspaces",
    EditOmsWorkspace: "Edit Log Analytics Workspace",
    EditProfile: "Edit Profile",
    ServicesHub: "Services Hub",
    Support: "Support",
    Selected: "Selected",
    Preview: "Preview",
    Menu: "Menu",
    Microsoft: "Microsoft",
    Services: "Services",
    ContractDetails: "Contract details",
    Contract: "Contract",
    LearnMore: "Learn more",
    NoAlerts: "You have no alerts",
    Operations: "Operations",
    AlertsTitle: "Alerts",
    SupportContacts: "Support contacts",
    ManageAccess: "Manage access",
    Profile: "Profile",
    Alert: "Alert",
    SeeAllAlerts: "See all alerts",
    OpenRequests: "Open requests",
    History: "History",
    ContactSupport: "Contact support",
    Recommended: "Recommended",
    Included: "Included",
    Catalog: "Catalog",
    Health: "Health",
    CriticalAlerts: "Critical alerts",
    ActiveAssessments: "Active assessments",
    NewAssessments: "New assessments",
    Learning: "Learning",
    LearningCampus: "Learning Campus",
    LearnOnDemand: "Learn on-demand",
    Workshops: "Workshops",
    Webcasts: "Webcasts",
    Features: "Features",
    Settings: "Settings",
    Search: "Search",
    UsersOperations: "Users",
    OptInOptOutFlyOutText: "If you change your mind and would like to reconsider visting the new site then just come over here and you will see the option to do so.",
    OptInOptOutGoBackFlyOutText: "Not that you would ever want to go back, but if you would like to revisit the old site just come over here and you will see the option to return.",
    Registrations: "Registrations",
    Office365Roadmap: "Office 365 Updates",
    Roles: "Roles",
    DeliveryArtifacts: "Shared files",
    AboutServicesHub: "About Services Hub",
    SupportRequests: "Support requests",
    UpdateCenter: "Update Center",
    ProductUpdates: "Product Updates",
    Notifications: "Notifications",
    SkipToMainContent: "Skip to main content",
    CustomerPulse: "Customer pulse",
    Plans: "Plans",
    GettingStarted: "Getting Started",
    Blog: "Blog",
    ContactUs: "Contact us",
    GiveFeedback: "Give feedback",
    Documentation: "Documentation",
    CustomerActivityPowerBI: "Customer Activity Power BI",
    Customers: "Customers",
    AddACustomer: "Add a customer",
    AdminCenter: "Admin Center",
    CreateAWorkspace: "Create a workspace",
    SessionExpired: "Session Expired",
    SessionExpiredText: "Your current authentication session has expired. Please log in again to access Services Hub.",
    Okay: "Okay",
    ResourceCenter: "Resource Center",
    ReleaseNotes: "Release Notes",
    ContactServicesHub: "Contact Services Hub"
}

export type IHeaderResx = typeof HeaderResx;