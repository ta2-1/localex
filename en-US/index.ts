import { MainResx, IMainResx } from "./main"
import { PseResx, IPseResx }  from "./pse"
import { InsightsResx, IInsightsResx }  from "./insights"
import { ProfileResx, IProfileResx }  from "./profile"
import { HeaderResx, IHeaderResx }  from "./header"
import { FooterResx, IFooterResx }  from "./footer"
import { UsersResx, IUsersResx }  from "./users"
import { ContactUsResx, IContactUsResx } from "./contactus"
import { FeedbackResx, IFeedbackResx } from "./feedback"
import { ErrorResx, IErrorResx } from "./error"
import { PublicResx, IPublicResx }  from "./public"
import { ServicesResx, IServicesResx }  from "./services"
import { AssessmentsResx, IAssessmentsResx }  from "./assessments"
import { PageTitlesResx, IPageTitlesResx } from "./pagetitles"
import { TagPickerResx, ITagPickerResx } from "./tagpicker"
import { SearchResx, ISearchResx } from "./search"
import { RegionConfigResx, IRegionConfigResx } from "./regionconfig"
import { QuestsResx, IQuestsResx } from "./quests"
import { LocationResx, ILocationResx } from "./location"
import { ContractResx, IContractResx } from "./contract"
import { QuestionnaireResx, IQuestionnaireResx } from "./questionnaire"
import { OperationsResx, IOperationsResx } from "./operations"
import { UpdateCenterResx, IUpdateCenterResx } from "./updatecenter"
import { ContractDetailsResx, IContractDetailsResx} from "./contractdetails"
import { OnboardingResx, IOnboardingResx} from "./onboarding"
import { MwfCoreResx, IMwfCoreResx} from "./mwfcore"
import { DataboardResx, IDataboardResx} from "./databoard"
import { ServicesCatalogResx, IServicesCatalogResx } from "./servicescatalog"
import { SettingsResx, ISettingsResx } from "./settings"
import { WorkspacesResx, IWorkspacesResx } from "./workspaces"
import { RegistrationsResx, IRegistrationsResx } from "./registrations"
import { DeliveryArtifactsResx, IDeliveryArtifactsResx } from "./deliveryartifacts"
import { RolesResx, IRolesResx } from "./roles"
import { SocialFeedResx, ISocialFeedResx } from "./socialfeed"
import { PaginationResx, IPaginationResx } from "./pagination"
import { SupportRequestsResx, ISupportRequestsResx } from "./supportrequests"
import { WebcastsResx, IWebcastsResx } from "./webcasts"
import { PlansResx, IPlansResx } from "./plans"
import { CustomerPulseResx, ICustomerPulseResx } from "./customerpulse"
import { HealthIndexResx } from "./healthindex"
import { CustomerResx, ICustomerResx } from "./complexCustomer"
import { BreadcrumbResx, IBreadcrumbResx } from "./breadcrumb"
import { ManageUsersResx, IManageUsersResx } from "./manageusers"
import { SearchResultsResx, ISearchResultsResx } from "./searchresults"
import { CreateSupportResx, ICreateSupportResx } from "./createsupport"
import { SupportCaseContactResx, ISupportCaseContactResx } from "./supportcasecontact"
import { SupportPhoneNumbers } from "./supportphonenumbers"
import { ManageSupportResx, IManageSupportResx } from "./managesupport"
import { CaseDetailsResx, ICaseDetailsResx } from "./CaseDetails"
import { GroupResx, IGroupResx } from "./group"

export var Resources =  {
    Main : MainResx,
    Pse: PseResx,
    Insights: InsightsResx,
    Profile: ProfileResx,
    Header: HeaderResx,
    Footer: FooterResx,
    Users: UsersResx,
    ContactUs: ContactUsResx,
    Feedback: FeedbackResx,
    Public: PublicResx,
    Error: ErrorResx,
    Assessments: AssessmentsResx,
    Services: ServicesResx,
    PageTitles: PageTitlesResx,
    TagPicker: TagPickerResx,
    Search: SearchResx,
    RegionConfig: RegionConfigResx,
    Quests: QuestsResx,
    Contract: ContractResx,
    Location: LocationResx, 
    Questionnaire: QuestionnaireResx, 
    Operations: OperationsResx,
    UpdateCenter: UpdateCenterResx,
    ContractDetails: ContractDetailsResx,
    Onboarding: OnboardingResx,
    MwfCore: MwfCoreResx,
    Databoard: DataboardResx, 
    ServicesCatalog: ServicesCatalogResx,
    Settings: SettingsResx,
    Workspaces: WorkspacesResx,
    Registrations: RegistrationsResx,
    DeliveryArtifacts: DeliveryArtifactsResx,    
    Roles: RolesResx,
    SocialFeed: SocialFeedResx,
    Pagination: PaginationResx,
    SupportRequests: SupportRequestsResx,
    Webcasts: WebcastsResx,
    Plans: PlansResx,
    CustomerPulse: CustomerPulseResx,
    HealthIndex: HealthIndexResx,
    Customer: CustomerResx,
    Breadcrumb: BreadcrumbResx,
    ManageUsers: ManageUsersResx,
    SearchResults: SearchResultsResx,
    CreateSupport: CreateSupportResx,
    SupportCaseContact: SupportCaseContactResx,
    SupportPhoneNumbers: SupportPhoneNumbers,
    ManageSupport: ManageSupportResx,
    CaseDetails: CaseDetailsResx,
    Group: GroupResx
}

export type IResources = typeof Resources;