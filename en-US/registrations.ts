﻿export var RegistrationsResx = {
    Registrations: "Registrations",
    Id: "Id",
    Created: "Created",
    LastUpdated: "LastUpdated",
    CreatedById: "CreatedById",
    RecipientEmail: "RecipientEmail",
    WorkspaceId: "WorkspaceId",
    RedeemedBy: "RedeemedBy",
    RedeemedDate: "RedeemedDate",
    LastInviteSent: "LastInviteSent",
    RevokedDate: "RevokedDate",
    RevokedBy: "RevokedBy",
    SearchError: "Please enter a search term and select a filter",
    Ascend: "Ascend",
    Descend: "Descend",
    SearchTerm: "Search Term",
    Filter: "Filter",
    Sort: "Sort",
    SelectFilter: "Select Filter",
    SelectSort: "Select Sort",
    SearchNotFoundMessage: "Sorry, no matches found for your search. Please try again."
}

export type IRegistrationsResx = typeof RegistrationsResx;