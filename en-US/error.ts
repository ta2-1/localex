﻿var ConstValues = {
    ContactSupport: "If the failure persists, please contact support by emailing serviceshubteam@ppas.uservoice.com.",
    TryAgain: "Please try again."
}

export var ErrorResx = {
    Error: "Error",
    Errors: "Errors",
    NotAuthorized: "Sorry, we were not able to find your contract information. Contact your Microsoft representative or send us an <a id='Error-NotAuthorized-mailto' href='mailto:serviceshubteam@ppas.uservoice.com?subject=Need help signing in to the Microsoft Services Hub website&body=Please describe your question or problem here.%0A%0A%0A%0ANote, Microsoft Services Hub uses UserVoice for website questions and help.'>email</a> if you require further assistance.",
    NotAuthorized_Header: "Sorry, we're unable to sign you in at this time.",
    FailedToRedeem: "The invitation link may have already been used, is invalid, or has expired.  Contact your Microsoft representative or send us an <a ng-attr-id='Error-FailedToRedeem-mailto' href='mailto:serviceshubteam@ppas.uservoice.com?subject=Need invitation help for the Microsoft Services Hub website&body=Please describe your question or problem here.%0A%0A%0A%0ANote, Microsoft Services Hub uses UserVoice for website questions and help.'>email</a> if you require further assistance.",
    FailedToRedeem_Header: "Sorry, we're unable to accept this invitation.",
    InvalidRegistrationFormat: "Invalid registrationId format",
    InvalidRegistrationEmail: "This invitation must be redeemed using the email address the invitation was originally sent to.  Please log in using the correct email address.  Contact your Microsoft representative or send us an <a id='Error-InvalidRegistrationEmail-mailto' href='mailto:serviceshubteam@ppas.uservoice.com?subject=Need invitation help for the Microsoft Services Hub website. Email does not match.&body=Please describe your question or problem here.%0A%0A%0A%0ANote, Microsoft Services Hub uses UserVoice for website questions and help.'>email</a> if you require further assistance.",
    InvalidRegistrationEmail_Header: "Sorry, we're unable to accept this invitation.",
    InvalidSearchTerm: "Invalid search term supplied.",
    NoWorkspace: "You must be a registered user to access this website. Look for an invitation email with a link to register. Or, if you have already registered, make sure you are signing in with the same account that you used before.<br/><br/>Contact your Microsoft representative or send us an <a ng-attr-id='Error-NoWorkspace-mailto' href='mailto:serviceshubteam@ppas.uservoice.com?subject=Need help signing into the Microsoft Services Hub website&body=Please describe your question or problem here.%0A%0A%0A%0ANote, Microsoft Services Hub uses UserVoice for website questions and help.'>email</a> if you need further assistance.",
    NoWorkspace_Header: "Registration required",
    ExistingAccountWithSameEmailAddress: "You already have an existing account with the same email address.  Please try again with that account.",
    ExistingAccountWithSameEmailAddress_Header: "Sorry, we're unable to sign you in with this account.  This can happen when you have a work and personal account associated with the same email address.",

    RequiresWorkAccount: "Services Hub requires you to logon with your <a id='Error-NoMSA' href='https://azure.microsoft.com/en-us/pricing/member-offers/msdn-benefits-details/work-accounts-faq/'>work account</a>.",
    WorkAccountDescription: "This may be an Azure Active Directory, Office 365 or similar account.  Microsoft Accounts (also known as Live ID) are not supported.",
    MSABlockContactUs: "If you need further assistance, please <a id='Error-NotAuthorized-mailto' href='mailto:serviceshubteam@ppas.uservoice.com?subject=MSA sign-in help to Services Hub&body=Please describe your question or problem here. Someone from the team will reply back as soon they can.%0A%0A%0A%0ANote, Microsoft Services Hub uses UserVoice for questions and help.'>contact us</a>.",
    TryRegister: "Try to register",
    TryToRegister2: " while using a work account.",
    AlreadyRegistered: "If you have already registered with your work account, please try to sign in again using that.",
    SupportContactManagementContactTam: "This operation failed.  Please contact your Microsoft technical account manager or support account coordinator to help reassign your support contacts.",
    SupportContactManagementMaxContacts: "You have reached the maximum allowed support contacts. You will need to un-designate someone before you can add another.",
    SupportContactManagementMaxContactsContactTam: "You have reached the maximum allowed support contacts. You will need to contact your Microsoft technical account manager or support account coordinator to help reassign your support contacts.",
    SupportContactManagementUnknownError: "An unknown error occurred.  Please try this operation again.",
    GetAllSupportContactRolesForUserFailed: "Something went wrong while loading Access-IDs. Please refresh the page to try again. " + ConstValues.ContactSupport,
    SignupServiceTamNotFound: "Contact your Microsoft Representative to get the Assessment Setup Service requested",

    CookieDisclaimer: "By using this site, you agree to the use of cookies for analytics and personalized content.",

    AssessmentsInvalidWorkspaceName: "The Azure Log Analytics Workspace Name must be between 4 and 63 characters, and can contain only letters, numbers and '-'. The '-' shouldn't be the first or the last symbol.",
    AssessmentsInvalidResourceGroupName: "The Azure Resource Group name must be between 1 and 90 characters, and can only include alphanumeric characters, periods, underscores, hyphens and parenthesis and cannot end in a period.",
    AssessmentsInvalidLocation: "The Azure resource location must be specified",
    AssessmentsSubscriptionDisabled: "{0}: The selected Azure subscription is disabled. Please re-enable the subscription and then refresh the page to try again, or choose another subscription",
    AssessmentsWorkspaceNameNotUnique: "{0}: The workspace name already exists in a different subscription or resource group.",
    AssessmentsResourceGroupNameExists: "{0}: The resource group name already exists in a different region",
    AssessmentsEnableSolutionFailed: "{0}: You do not have permission to update the selected workspace and/or resource group. Please contact your administrator to acquire the permission.",
    AssessmentsNotAuthorized: "{0}: You do not have permission to create workspaces or resource groups with the selected subscription. Please contact your administrator to acquire the permission.",
    OmsWorkspaceRegionNotSupported: "{0}: The region {1} of the selected Azure Log Analytics workspace is currently not supported.",
    AssessmentsUnknownError: "{0}: Something went wrong. Please try again or <a id='assessments-ContactUs' href='mailto:serviceshubteam@ppas.uservoice.com?subject=Question%20about%20linking%20my%20Azure%20subscription&body=Please%20describe%20your%20question%20or%20problem%20here.%20Someone%20from%20the%20team%20will%20reply%20back%20as%20soon%20they%20can.%0A%0A%0A%0ANote,%20Microsoft%20Services%20Hub%20uses%20UserVoice%20for%20website%20questions%20and%20help'> contact us.</a>",
    AssessmentsNoWorkspaceSelectedError: "No workspace selected. A workspace is mandatory for linking to an existing workspace.",
    AssessmentsNoSubscriptionSelectedError: "No subscription selected. A subscription is mandatory for linking to a workspace.",

    AssessmentsGetSubscriptionsIssue: "Something went wrong while getting your Azure subscriptions. Please try again or <a id='assessments-ContactUs' href='mailto:serviceshubteam@ppas.uservoice.com?subject=Question%20about%20linking%20my%20Azure%20subscription&body=Please%20describe%20your%20question%20or%20problem%20here.%20Someone%20from%20the%20team%20will%20reply%20back%20as%20soon%20they%20can.%0A%0A%0A%0ANote,%20Microsoft%20Services%20Hub%20uses%20UserVoice%20for%20website%20questions%20and%20help'> contact us.</a>",
    AssessmentsGetResourceGroupIssue: "Something went wrong while getting your Azure resource group. Please try again or <a id='assessments-ContactUs' href='mailto:serviceshubteam@ppas.uservoice.com?subject=Question%20about%20linking%20my%20Azure%20subscription&body=Please%20describe%20your%20question%20or%20problem%20here.%20Someone%20from%20the%20team%20will%20reply%20back%20as%20soon%20they%20can.%0A%0A%0A%0ANote,%20Microsoft%20Services%20Hub%20uses%20UserVoice%20for%20website%20questions%20and%20help'> contact us.</a>",
    AssessmentsGetOMSWorkspacesIssue: "Something went wrong while getting your Log Analytics workspace. Please try again or <a id='assessments-ContactUs' href='mailto:serviceshubteam@ppas.uservoice.com?subject=Question%20about%20linking%20my%20Azure%20subscription&body=Please%20describe%20your%20question%20or%20problem%20here.%20Someone%20from%20the%20team%20will%20reply%20back%20as%20soon%20they%20can.%0A%0A%0A%0ANote,%20Microsoft%20Services%20Hub%20uses%20UserVoice%20for%20website%20questions%20and%20help'> contact us.</a>",

    SubscriptionOrWorkspaceNotFoundIssue: "{0}: Something went wrong and we could not find your linked Log Analytics workspace or your Azure subscription. Please log into Azure Portal and verify their presence. To resolve this issue, click 'Edit Log Analytics Workspace' and link again.",

    ContractDetailsUnknownError: "An unknown error occurred. " + ConstValues.TryAgain,

    OmsAddUserError: "{0}: Something went wrong adding the user to Log Analytics. Please add through the Azure portal <a id='assessments-ContactUs' href='https://aka.ms/adduserstola'> click here </a>",
    OmsAddUserErrorTryAgain: "{0}: Something went wrong when trying to add a user in Azure Log Analytics. " + ConstValues.TryAgain,
    OmsRemoveUserError: "{0}: Something went wrong when trying to remove a user in Azure Log Analytics. If necessary, remove the user directly from the Azure Portal. " + ConstValues.ContactSupport,
    OmsRemoveUserErrorTryAgain: "{0}: Something went wrong when trying to remove a user in Azure Log Analytics. " + ConstValues.TryAgain,

    SearchUsersError: "Something went wrong while searching users. " + ConstValues.ContactSupport,

    RemoveWorkspaceForUserError: "Something went wrong when trying to remove a workspace for user. " + ConstValues.TryAgain,
    AddWorkspaceForUserError: "Something went wrong when trying to add a workspace for user. " + ConstValues.TryAgain,
    RemoveOnlyWorkspaceForUserError: "The workspace can not be removed, As this is the only workspace user has.",

    InvalidContractSearchTerm: "Invalid search term supplied.",

    PageLoadingError: "This page could not be displayed. " + ConstValues.TryAgain + " If the failure persists, please contact support.",

    SmcPasswordChangeError: "Could not update your password at this time. If the failure persists, please contact support.",
    RemoveGroupError: "Could not remove the group at this time. If the failure persists, please contact support.",
    SmcPasswordGetError: "Could not get your password at this time. If the failure persists, please contact support.",

    UpdateRolesForUserError: "Something went wrong when trying to update roles for user. " + ConstValues.TryAgain,
    EnvironmentDisclaimer: "This environment, dev, is used for development purposes only. ABSOLUTELY NO CUSTOMER DATA ALLOWED AT ANY TIME.",

    SurveyUnknownError: "The survey could not be shown. " + ConstValues.TryAgain + " If the failure persists, please contact support.",

    RegisterWebcastError: "Something went wrong when trying to register for a webcast. " + ConstValues.TryAgain + " If the failure persists, please contact support.",

    LoadPlansFailed: "Something went wrong loading plans for this workspace. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    LoadPlanFailed: "Something went wrong loading details for this plan. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    LoadMembersFailed: "Something went wrong loading members for this plan. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    SavePlanFailed: "Something went wrong saving changes for this plan. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    DeletePlanFailed: "Something went wrong deleting this plan. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    LoadTaskFailed: "Something went wrong loading details for this task. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    AddTaskFailed: "Something went wrong adding this task. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    CloneTaskFailed: "Something went wrong cloning this task. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    SaveTaskFailed: "Something went wrong saving changes for this task. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    DeleteTaskFailed: "Something went wrong deleting this task. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    AddNoteFailed: "Something went wrong adding this note. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    SaveNoteFailed: "Something went wrong saving this note. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    DeleteNoteFailed: "Something went wrong deleting this note. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    AddMemberFailed: "Something went wrong adding this member. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    RemoveMemberFailed: "Something went wrong removing this member. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    ToggleMemberRoleFailed: "Something went wrong changing the role for this member. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    LoadContractsFailed: "Something went wrong loading the contracts for the current user. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    LoadSchedulesFailed: "Something went wrong loading the schedules for the selected contract. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,

    CreateAssessmentRemediationPlanNoDataAvailable: "There is no assessment data available. You can continue to create a new plan without assessment data, or go back and try again.",
    CreateAssessmentRemediationPlanImportError: "There were issues encountered during import, and the task list may not be complete. Try to create the plan again. " + ConstValues.ContactSupport,

    addUserPulseAccessError: "Something went wrong when giving the user access to customer pulse. " + ConstValues.TryAgain + " If the failure persists, please contact support.",
    removeUserPulseAccessError: "Something went wrong when removing user's access to customer pulse. " + ConstValues.TryAgain + " If the failure persists, please contact support.",
    removeUserError: "{0}: Something went wrong when removing user from workspace. " + ConstValues.TryAgain + " If the failure persists, please contact support.",
    removeInvitationError: "{0}: Something went wrong when removing invitation from workspace. " + ConstValues.TryAgain + " If the failure persists, please contact support.",

    GetConfiguredSurveysFailed: "Something went wrong loading available surveys. " + ConstValues.TryAgain + " If the failure persists, please contact support.",
    GetAssessmentCatalogConfigurationFailed: "Something went wrong loading assessment catalog configuration. " + ConstValues.TryAgain + " If the failure persists, please contact support.",
    GetAssessmentCatalogServiceGroupFailed: "Something went wrong loading assessment catalog service group. " + ConstValues.TryAgain + " If the failure persists, please contact support.",
    LoadingContactRolesError: "Something went wrong while loading roles. " + ConstValues.TryAgain + " If the failure persists, please contact support.",
    LoadCustomerFailed: "Something went wrong while loading this customer. " + ConstValues.ContactSupport,
    AddCustomerFailed: "Something went wrong while adding this customer. " + ConstValues.ContactSupport,
    EditCustomerFailed: "Something went wrong while updating this customer. " + ConstValues.ContactSupport,
    RemoveContractsFailed: "Something went wrong while removing contracts from this customer. " + ConstValues.ContactSupport,
    GetSupportContactsFailed: "Something went wrong while loading support contacts from the contract. " + ConstValues.ContactSupport,

    CreateWorkspaceFailed: "Something went wrong while creating a workspace for this customer. " + ConstValues.ContactSupport,
    DeleteWorkspaceFailed: "Something went wrong while removing the selected workspaces from this customer. " + ConstValues.ContactSupport,

    LoadGlobalAdminDataFailed: "Something went wrong while loading the list of global administrators for this customer. " + ConstValues.ContactSupport,
    AddGlobalAdminFailed: "Something went wrong while trying to add global administrators to this customer. " + ConstValues.ContactSupport,
    DeleteGlobalAdminFailed: "Something went wrong while removing the selected global administrators from this customer. " + ConstValues.ContactSupport,
    ResendInviteFailed: "Something went wrong while resending an invite email for the selected global administrator. " + ConstValues.ContactSupport,

    addMsaExceptionError: "Something went wrong when allowing Microsoft Accounts. " + ConstValues.TryAgain + " If the failure persists, please contact support.",
    removeMsaExceptionError: "Something went wrong when disallowing Microsoft Accounts. " + ConstValues.TryAgain + " If the failure persists, please contact support.",
    getMsaExceptionStatusForCurrentWorkspaceError: "Something went wrong while looking up whether Microsoft Accounts should have access to the current workspace. " + ConstValues.TryAgain + " If the failure persists, please contact support.",

    // support case creation 
    GetProductsError: "Something went wrong getting products " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    GetProductTreeError: "Something went wrong getting product tree. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    GetLocaleInformationError: "Something went wrong getting locale information. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,

    GetManageableRoleAndPermissionNamesForCurrentWorkspaceFailed: "Something went wrong getting all manageable roles for this workspace. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    InviteUsersToCurrentWorkspaceFailed: "Something went wrong inviting users to this workspace. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    EditUsersInCurrentWorkspaceFailed: "Something went wrong editing users in this workspace. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    ResendInviteUsersToCurrentWorkspaceFailed: "Something went wrong when resending this invite. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,

    InviteUsersModalOnInitFailed: "Something went wrong while loading the invite users dialogue. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    GetRolesAndPermissionsByUserForCurrentWorkspaceFailed: "Something went wrong while loading roles and permissions for the current user. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,

    ToggleCustomerAdminFailed: "Something went wrong while adding/removing the current user to/from the selected customer. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,

    GetGroupListPaginatedFailed: "Something went wrong while loading list of groups in workspace. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    GetTokenListPaginatedFailed: "Something went wrong while loading list of users in workspace. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    GetCurrentWorkspaceRegistrationsByRolePaginatedFailed: "Something went wrong while loading list of users in workspace. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    LoadPaginatedUnregisteredDataFailed: "Something went wrong while loading list of unregistered users in workspace. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    GetCustomerGlobalAdminRegistrationsForCurrentWorkspacePaginatedFailed: "Something went wrong while loading list of admins in customer. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,

    CsvBulkInviteMaxNumberOfEmailsReachedFormatString: "You have reached the maximum number of allowed emails for a single invite. Please limit your invitation to a maximum of {0} users at a time.",
    CsvBulkInviteNoFileString: "Please provide a valid CSV file.",
    CsvBulkInviteParseEmailErrorFormatString: "Line number: {0}: Something went wrong reading the email address: {1}",
    CsvBulkInviteNoEmailsFound: "We could not find any email addresses in the uploaded file. Please try to upload the CSV file again.",
    CsvBulkInviteNoStartStringFoundString: "We could not find any email addresses in the uploaded file. Please use our provided CSV template and insert your email addresses starting at line 23.",

    GetAssessmentEntitlementOverrideError: "Something went wrong trying to get assessment entitlement override. " + ConstValues.TryAgain,
    SetAssessmentEntitlementOverrideError: "Something went wrong trying to set assessment entitlement override. " + ConstValues.TryAgain,
    RemoveAssessmentEntitlementOverrideError: "Something went wrong trying to remove assessment entitlement override. " + ConstValues.TryAgain,

    CreateCaseError: "There was an issue with your submission." + ConstValues.TryAgain + "If the failure persists, please try creating the case using preferred phone number or contact support by emailing serviceshubteam@ppas.uservoice.com.",

    GetCaseDetailsError: "Something went wrong trying to get case details. " + ConstValues.ContactSupport,
    GetCaseDetailsNotFoundError: "Could not find the case details. " + ConstValues.ContactSupport,
    GetCaseDetailsForbiddenError: "You do not have permission to view case details. " + ConstValues.ContactSupport,
    
    //Group Errors
    CreateGroupFailed: "Something went wrong trying to create the group. " + ConstValues.ContactSupport,
    RemoveGroupsFailed: "Something went wrong trying to remove the groups. " + ConstValues.TryAgain,
    UpdateGroupFailed: "Something went wrong trying to update the group. " + ConstValues.TryAgain,
    LoadingGroupFailed: "Something went wrong trying to load groups. " + ConstValues.TryAgain,
    RemoveUserFromGroupFailed: "Something went wrong while trying to remove the user from the group. " + ConstValues.TryAgain,

    GetRolesAndClaimsFailed: "Something went wrong while trying to get the roles and claims for a user. " + ConstValues.TryAgain,
    UnableToRemoveFrom: "Unable to remove from: {0}",

    PreferredEmailPendingVerification: "You have a pending email verification. Please check your inbox or resend the email on the profile page.",

    ResendEmailVerificationFailed: "Something went wrong trying to resend the verification email. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    EmailValidationFailed: "Something went wrong trying to verify your preferred email. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,

    SavingDefaultGroupFailed: "Something went wrong trying to save default groups. " + ConstValues.TryAgain + " " + ConstValues.ContactSupport,
    // default value
    Default: "Oops. Something went wrong!"
}

export type IErrorResx = typeof ErrorResx;