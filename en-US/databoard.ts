﻿export var DataboardResx = {
    MoreThan300ServicesAvailable: "Available services",
    ContractDetails: "Contract details",
    NewCases: "new cases",
    CurrentOpenSupportRequests: "Active support requests",
    TotalNumberOfSupportRequests: "Total support requests",
    Support: "Support",
    NumberOfSrs: "Number of support requests last year",
    NumberOfOpenSrs: "Number of open support requests",
    ContactSupport: "Contact Microsoft Support",
    TotalSrs: "Total support requests",
    ProactiveServices: "Proactive Services",
    NumberOfServices: "Number of services consumed",
    ExploreServices: "Explore all services",
    IncludedServices: "Explore included services",
    PremierEnterpriseAgreement: "Premier Enterprise Agreement",
    MoreThanLastWeek: "more than last week",
    LessThanLastWeek: "less than last week",
    ViewAll: "View all",
    ServiceRequest: "Service Request",
    SupportRequests: "Support Requests",
    OpenSupportRequests: "Open Support Requests",
    AvailableServices: "Available Services",
    LearningActivitiesConsumedLastYear: "Learning on-demand courses consumed",
    TotalServicesConsumedLastYear: "Total consumed services",
    MoreThanLastMonth: "more than last month",
    SeverityWithColon: "Severity:",
    InternalSupportContact: "Internal support contact",
    CustomerSupportManager: "Services Admin", // "Services Admin" should not be localized
    TechnicalAccountManager: "Technical account manager",
    BackupTechnicalAccountManager: "Backup technical account manager",
    Microsoft: "Microsoft",
    DurationWithColon: "Duration:",
    LastExecutionWithColon: "Last Execution:",
    OnDemand: "On-Demand",
    AssessmentServiceMoreDetails: "https://aka.ms/assessment_setup_service",
    AssessmentSignupEmail: "oda-setup-request@microsoft.com",
    AssessmentSignupMailtoSubject: "Request to schedule Services Hub On-demand assessments set-up and configuration service",
    AssessmentSignupMailtoBody: "Hello,\r\n\r\nI would like to have a PFE to help out setup the On-Demand Assessments under the Unified Support Contract. \r\n" +
        "The Azure Subscription Owner/IT Admin can be contacted at <email id of IT Admin>\r\n" +
        "Available time slot for a call:\r\n" +
        "   1. Date and Time and Time zone \r\n" +
        "   2. Date and Time and Time zone \r\n" +
        "   3. Date and Time and Time zone\r\n\r\n",
    AssessmentSignupEmailV2: "sacteam@microsoft.com",
    AssessmentSignupMailtoSubjectV2: "Request for On-Demand Assessment - Setup and Configuration Service",
    AssessmentSignupMailtoBodyV2:
        "Hello,\r\n" +
        "I would like to have a Microsoft Engineer's assistance in setting up one or more On-Demand Assessments.\r\n\r\n" +
        "I plan to:\r\n\r\n" +
        "( ) Use my organization’s Azure Subscription. If yes, here is my Azure Subscription Owner’s Email address:\r\n" +
        "( ) Use a $10 complementary Azure Sponsorship. If yes, here is the registered user’s email address they want associated with the Sponsorship:\r\n" +
        "Listed below are the On-Demand Assessments I would like configured and what is the email for each IT Admin associated with each assessment’s technology:  (list assessments and email addresses)",
    OnDemandAssessmentName: "Setup & Configuration",
    OnDemandAssessmentTypeName: "Included Unified Benefit: On-Demand Assessment",
    Upcoming: "Upcoming",
    Recommendations: "Recommendations",
    ActionCenter: "Action Center",
    StartedWithColon: "Started:",
    KeepUpTheGreatWork: "Keep up the great work!",
    IfThereIsAnything: "If there is anything we can help you with...",
    ContactMicrosoftSupport: "Contact Microsoft Support",
    ViewSupportHistory: "View support history",
    NoServicesPlanned: "No active services",
    YouHaveServices: "You have more than 300 services included in your contract",
    DiscoverServices: "Discover included services",
    PendingConfiguration: "Pending configuration",
    TAMRecommendation: "Recommended by TAM",
    ServiceRecommendation: "Recommended for you",
    CriticalSituationAlert: "Critical alert",
    AdHoc: "Recommended action",
    ActNow: "Act now",
    NoContacts: "No contacts at this time",
    IfYouWouldLikeToManage: "If you would like to add contacts...",
    ManageSupportContacts: "Manage support contacts",
    NoAvailableData: "No available data",
    InsiderMessage: "Thank You!",
    Hours: "hours",
    Days: "days",
    DaysAgo: "days ago",
    MyCompany: "My Company",
    Other: "Other",
    Product: "Product",
    NumberOfRequests: "Number of Requests",
    ServiceCategory: "Service Category",
    TotalServices: "Number of Services",
    SignUp: "Sign up",
    MoreDetails: "More Details"
}
export type IDataboardResx = typeof DataboardResx;