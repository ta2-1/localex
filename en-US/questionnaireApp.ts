﻿import {QuestionnaireResx, IQuestionnaireResx}  from "./questionnaire"

export var Resources = {
    Questionnaire: QuestionnaireResx,
}

export type IResources = typeof Resources;