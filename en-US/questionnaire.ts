﻿export var QuestionnaireResx = {
    Complete: "complete",
    PreviousQuestion: "Previous question",
    NextQuestion: "Next question",
    Evaluate: "Evaluate your Active Directory disaster recovery plan",
    EvaluateDescriptionFirst: "This questionnaire will ask some questions that will assess the current state of your Active Directory disaster recovery plan and provide you some guidance on how you can improve it.",
    EvaluateDescriptionSecond: "This 5 minute exercise does not require access to your Active Directory server instances and will not have any effect on your servers.",
    Start: "Start",
    WeHaveInsights: "Great! We have some insights for you.",
    Over: "over",
    RightPolicy: "Right back-up policy",
    OptimizedQueries: "Optimized queries",
    PendingAssessment: "Pending SQL Server assessment",
    TempdbContention: "Tempdb pagelatch contention",
    MemoryPressure: "Memory pressure",
    WouldYouLikeToImprove: "Would you like to improve this score?",
    Included: "Included",
    Free: "Free",
    YourScore: "Your score",
    QuestionnaireInsights: "Questionnaire Insights",
    Microsoft: "Microsoft",
    RadioGroup: "Survey radio buttons"
}
export type IQuestionnaireResx = typeof QuestionnaireResx;